import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-comment-outline-icon',
  templateUrl: './comment.svg',
  styleUrls: ['./comment-outline-icon.component.scss']
})
export class CommentOutlineIconComponent {
  @Input() color: 'white' | 'black' | string = 'white';
}
