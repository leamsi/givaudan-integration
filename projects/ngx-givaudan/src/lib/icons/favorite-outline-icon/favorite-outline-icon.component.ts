import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-favorite-outline-icon',
  templateUrl: './favorite-outline.svg',
  styleUrls: ['./favorite-outline-icon.component.scss']
})
export class FavoriteOutlineIconComponent {
  @Input() color: 'white' | 'black' | string = 'white';
}
