import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-filter-icon',
  templateUrl: './filter.svg',
  styleUrls: ['./filter-icon.component.scss']
})
export class FilterIconComponent {
  @Input() width: '12px' | '' = '12px';
  @Input() color: 'black' = 'black';

}
