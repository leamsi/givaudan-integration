import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-arrow-forward-icon',
  templateUrl: './arrow-forward.svg',
  styleUrls: ['./arrow-forward-icon.component.scss']
})
export class ArrowForwardIconComponent {
  @Input() color: '#549DAA' | 'black' = 'black';
  @Input() rotate: boolean = false;
}
