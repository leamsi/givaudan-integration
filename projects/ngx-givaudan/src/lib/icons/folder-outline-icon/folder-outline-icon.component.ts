import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-folder-outline-icon',
  templateUrl: './folder-outline.svg',
  styleUrls: ['./folder-outline-icon.component.scss']
})
export class FolderOutlineIconComponent {
  @Input() color: 'white' | 'black' | '#373737' | string = 'white';
}
