import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-help-circle-icon',
  templateUrl: './help-circle.svg',
  styleUrls: ['./help-circle-icon.component.scss']
})
export class HelpCircleIconComponent {
  @Input() color: 'white' | 'black' = 'white';
}
