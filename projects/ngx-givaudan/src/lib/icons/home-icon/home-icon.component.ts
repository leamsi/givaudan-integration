import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-home-icon',
  templateUrl: './home.svg',
  styleUrls: ['./home-icon.component.scss']
})
export class HomeIconComponent {
  @Input() color: '#549DAA' | 'white' = 'white';
}
