import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-search-icon',
  templateUrl: './search.svg',
  styleUrls: ['./search-icon.component.scss']
})
export class SearchIconComponent {
  @Input() color: 'white' | 'black' | string = 'black';
}
