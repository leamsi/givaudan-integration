import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-chevron-down-icon',
  templateUrl: './chevron-down.svg',
  styleUrls: ['./chevron-down-icon.component.scss']
})
export class ChevronDownIconComponent {
  @Input() color: 'white' | 'black' | '#549DAA' | '#69C4D4' = 'white';
  @Input() direction: 'up' | 'right' | 'down' | 'left' = 'down';
}
