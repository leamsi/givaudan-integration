import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent {
  @Input() color: 'white' | 'black' | string = 'white';
  @Input() background: 'white' | 'transparent' | string = '#549DAA';
  @Input() width: '161px' | '132px' | string = '161px';
  @Input() label: string = '';
  @Input() disabled: boolean = false;
  @Output() clickEvent = new EventEmitter<boolean>();


  onclick() {
    this.clickEvent.emit(true);
  }
}
