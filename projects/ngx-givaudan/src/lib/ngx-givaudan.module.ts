import {NgModule} from '@angular/core';
import {SearchIconComponent} from './icons/search-icon/search-icon.component';
import {ChevronDownIconComponent} from './icons/chevron-down-icon/chevron-down-icon.component';
import {FavoriteOutlineIconComponent} from './icons/favorite-outline-icon/favorite-outline-icon.component';
import {ArrowForwardIconComponent} from './icons/arrow-forward-icon/arrow-forward-icon.component';
import {FolderOutlineIconComponent} from './icons/folder-outline-icon/folder-outline-icon.component';
import {HelpCircleIconComponent} from './icons/help-circle-icon/help-circle-icon.component';
import {FilterIconComponent} from './icons/filter-icon/filter-icon.component';
import {CommentOutlineIconComponent} from './icons/comment-outline-icon/comment-outline-icon.component';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {ButtonComponent} from './button/button.component';
import {NgChartsModule} from 'ng2-charts';
import {LineChartComponent} from './charts/line-chart/line-chart.component';
import {BarChartComponent} from './charts/bar-chart/bar-chart.component';
import {DoughnutChartComponent} from './charts/doughnut-chart/doughnut-chart.component';
import {BubbleChartComponent} from './charts/bubble-chart/bubble-chart.component';
import {FooterComponent} from './footer/footer.component';
import {HeaderComponent} from './header/header.component';
import {SelectInputComponent} from './inputs/select-input/select-input.component';
import {InputComponent} from './inputs/input/input.component';
import {ArticleCallToActionComponent} from './articles/article-call-to-action/article-call-to-action.component';
import {HomeIconComponent} from './icons/home-icon/home-icon.component';
import {ArticlePreviewComponent} from './articles/article-preview/article-preview.component';

@NgModule({
  declarations: [
    SearchIconComponent,
    ChevronDownIconComponent,
    FavoriteOutlineIconComponent,
    CommentOutlineIconComponent,
    ArrowForwardIconComponent,
    HelpCircleIconComponent,
    FolderOutlineIconComponent,
    HomeIconComponent,
    FilterIconComponent,
    ButtonComponent,
    LineChartComponent,
    BarChartComponent,
    DoughnutChartComponent,
    BubbleChartComponent,
    FooterComponent,
    HeaderComponent,
    SelectInputComponent,
    InputComponent,
    ArticlePreviewComponent,
    ArticleCallToActionComponent,
  ],
  exports: [
    SearchIconComponent,
    ChevronDownIconComponent,
    FavoriteOutlineIconComponent,
    CommentOutlineIconComponent,
    ArrowForwardIconComponent,
    HelpCircleIconComponent,
    FolderOutlineIconComponent,
    HomeIconComponent,
    FilterIconComponent,
    ButtonComponent,
    LineChartComponent,
    BarChartComponent,
    DoughnutChartComponent,
    BubbleChartComponent,
    FooterComponent,
    HeaderComponent,
    SelectInputComponent,
    InputComponent,
    ArticlePreviewComponent,
    ArticleCallToActionComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    NgChartsModule
  ]
})
export class NgxGivaudanModule { }
