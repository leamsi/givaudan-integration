export const download = (document: Document, chartType: 'line' | 'bar' | 'doughnut' | 'bubble') => {
  function getChartNameAndPosition(chartType: 'line' | 'bar' | 'doughnut' | 'bubble') {
    switch (chartType) {
      case 'line':
        return {name: 'Line chart', position: 0};
      case 'bar':
        return {name: 'Bar chart', position: 1};
      case 'doughnut':
        return {name: 'Doughnut chart', position: 2};
      case 'bubble':
        return {name: 'Doughnut chart', position: 3};
    }
  }
  const link: HTMLAnchorElement = document.createElement('a');
  link.download = getChartNameAndPosition(chartType).name;
  link.href = document.getElementsByTagName('canvas')[getChartNameAndPosition(chartType).position].toDataURL();
  link.click();
}
