import {Component, EventEmitter, Inject, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {ChartConfiguration, ChartDataset, ChartEvent, ChartOptions, Point} from 'chart.js';
import {DOCUMENT} from '@angular/common';
import {download} from '../../functions';

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.scss'],
})
export class LineChartComponent implements OnChanges {
  @Input() chart!: {labels: string []; series: {label: string; data: number []} []};
  @Output() selectedEvent = new EventEmitter<{label: string, data: number}> ();

  private colors: {color: string; background: string} [] = [
    {color: '#36abe0', background: 'rgba(54,171,224,0.25)'},
    {color: '#FF50B9', background: 'rgba(255,80,185,0.25)'},
    {color: '#07e53a', background: 'rgba(7,229,58,0.25)'},
    {color: '#1607e5', background: 'rgba(22,7,229,0.25)'},
  ];

  private defaultChartDataSet:  ChartDataset<"line", (number | Point | null)> = {
    data: null,
    label: 'undefined',
    fill: true,
    tension: 0.5
  };

  public lineChartData: ChartConfiguration<'line'>['data'] = {} as ChartConfiguration<'line'>['data'];

  public lineChartOptions: ChartOptions<'line'> = {
    responsive: true,
    plugins: {
      legend: {
        display: true,
        position: 'top',
        reverse: false,
      },
      tooltip: {
        enabled: true,
        backgroundColor: 'white',
        titleColor: 'black',
        bodyColor: 'black',
        displayColors: false,
      },
    },
    scales: {
      y: {
        type: 'linear',
        position: 'left',
        grid: {
          display: true,
          color: 'red',
          drawOnChartArea: true,
        }
      },
      y1: {
        type: 'linear',
        display: true,
        position: 'right',
        grid: {
          drawOnChartArea: false,
        },
      },
    },
    elements: {
      point: {
        radius: 4,
        pointStyle: 'circle',
        borderWidth: 2,
        hoverBorderWidth: 2
      }
    }
  };

  lineChartPlugins = [{
    id: 'customCanvasBackgroundColor',
    beforeDraw: (chart: any, args: any, options: any) => {
      const {ctx} = chart;
      ctx.save();
      ctx.globalCompositeOperation = 'destination-over';
      ctx.fillStyle = options.color || 'black';
      ctx.fillRect(0, 0, chart.width, chart.height);
      ctx.restore();
    },
    afterDraw: (chart: any) => {
      if (chart.tooltip?._active?.length) {
        let x = chart.tooltip._active[0].element.x;
        let yAxis = chart.scales.y;
        let ctx = chart.ctx;
        ctx.save();
        ctx.beginPath();
        ctx.moveTo(x, yAxis.top);
        ctx.lineTo(x, yAxis.bottom);
        ctx.lineWidth = 2;
        ctx.strokeStyle = '#ffcc00';
        ctx.stroke();
        ctx.restore();
      }
    }
  }];

  constructor(@Inject(DOCUMENT) private document: Document) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['chart']) {
      this.lineChartData.labels = this.chart.labels;
      this.lineChartData.datasets = this.chart.series.map((serie, index) => {
          return {
            ...this.defaultChartDataSet,
            ...{
              label: serie.label,
              data: serie.data,
              borderColor: this.colors[index] ? this.colors[index].color : this.colors[0].color,
              pointBackgroundColor: this.colors[index] ? this.colors[index].color : this.colors[0].color,
              backgroundColor: this.colors[index] ? this.colors[index].background : this.colors[0].background
            }
          }
      });
    }
  }

  downloadChart() {
    download(this.document, 'line');
  }

  onChartClick($event: { event?: ChartEvent; active?: any[] }) {
    if ($event.active && $event.active[0]) {
      this.selectedEvent.emit({
        label: this.chart.labels[$event.active[0].index],
        data: this.chart.series[$event.active[0].datasetIndex].data[$event.active[0].index]
      });
    }
  }
}
