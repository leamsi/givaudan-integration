import {Component, Inject, ViewChild} from '@angular/core';
import {ChartConfiguration, ChartEvent, ChartOptions} from 'chart.js';
import {download} from '../../functions';
import {DOCUMENT} from '@angular/common';
import {BaseChartDirective} from 'ng2-charts';

@Component({
  selector: 'app-doughnut-chart',
  templateUrl: './doughnut-chart.component.html',
  styleUrls: ['./doughnut-chart.component.scss']
})
export class DoughnutChartComponent {
  doughnutData: ChartConfiguration<'doughnut'>['data'] = {
    labels: ['Man', 'Women'],
    datasets: [
      {
        label: 'Part',
        data: [1, 3],
        backgroundColor: [
          '#36abe0',
          '#FF50B9FC'
        ],
      }
    ]
  };
  doughnutOptions: ChartOptions<'doughnut'> = {}
  doughnutChartPlugins = [{
    id: 'customCanvasBackgroundColor',
    beforeDraw: (chart: any, args: any, options: any) => {
      const {ctx} = chart;
      ctx.save();
      ctx.globalCompositeOperation = 'destination-over';
      ctx.fillStyle = options.color || 'white';
      ctx.fillRect(0, 0, chart.width, chart.height);
      ctx.restore();
    }
  }];
  @ViewChild('chart') baseChartDirective!: BaseChartDirective;
  constructor(@Inject(DOCUMENT) private document: Document) {}

  downloadChart() {
    download(this.document, 'doughnut');
  }

  onChartClick($event: { event?: ChartEvent; active?: object[] }) {}
}
