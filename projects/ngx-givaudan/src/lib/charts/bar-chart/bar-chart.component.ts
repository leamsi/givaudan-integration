import {Component, Inject} from '@angular/core';
import {ChartConfiguration, ChartEvent, ChartOptions} from 'chart.js';
import {DOCUMENT} from '@angular/common';
import {download} from '../../functions';

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.scss']
})
export class BarChartComponent {

  public barChartData: ChartConfiguration<'bar'>['data'] = {
    labels: ['Apple', 'Strawberry'],
    datasets: [
      {
        label: 'My First Dataset',
        data: [1, 3],
        backgroundColor: '#36abe0',
        borderRadius: 5,
        hoverBorderColor: '#36abe0',
        hoverBorderWidth: 2,
        hoverBackgroundColor: 'white',
        barPercentage: 0.6
      }
    ]
  };
  public barChartOptions: ChartOptions<'bar'> = {};
  barChartPlugins = [{
    id: 'customCanvasBackgroundColor',
    beforeDraw: (chart: any, args: any, options: any) => {
      const {ctx} = chart;
      ctx.save();
      ctx.globalCompositeOperation = 'destination-over';
      ctx.fillStyle = options.color || 'white';
      ctx.fillRect(0, 0, chart.width, chart.height);
      ctx.restore();
    }
  }];
  constructor(@Inject(DOCUMENT) private document: Document) {}

  downloadChart() {
    download(this.document, 'bar');
  }

  onChartClick($event: { event?: ChartEvent; active?: object[] }) {

  }
}
