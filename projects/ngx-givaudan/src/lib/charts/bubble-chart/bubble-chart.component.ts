import {Component, Inject, ViewChild} from '@angular/core';
import {ChartConfiguration, ChartEvent, ChartOptions} from 'chart.js';
import {BaseChartDirective} from 'ng2-charts';
import {DOCUMENT} from '@angular/common';
import {download} from '../../functions';

@Component({
  selector: 'app-bubble-chart',
  templateUrl: './bubble-chart.component.html',
  styleUrls: ['./bubble-chart.component.scss']
})
export class BubbleChartComponent {
  public bubbleChartData: ChartConfiguration<'bubble'>['data'] = {
    datasets: [
      {
        label: 'My First Dataset',
        data: [{
          x: 20,
          y: 30,
          r: 15
        }, {
          x: 40,
          y: 10,
          r: 10
        }],
        backgroundColor: '#fad98c',
      }
    ]
  };
  public bubbleChartOptions: ChartOptions<'bubble'> = {};
  bubbleChartPlugins = [{
    id: 'customCanvasBackgroundColor',
    beforeDraw: (chart: any, args: any, options: any) => {
      const {ctx} = chart;
      ctx.save();
      ctx.globalCompositeOperation = 'destination-over';
      ctx.fillStyle = options.color || 'white';
      ctx.fillRect(0, 0, chart.width, chart.height);
      ctx.restore();
    }
  }];
  @ViewChild('chart') baseChartDirective!: BaseChartDirective;
  constructor(@Inject(DOCUMENT) private document: Document) {}

  downloadChart() {
    download(this.document, 'bubble');
  }

  onChartClick($event: { event?: ChartEvent; active?: object[] }) {

  }
}
