import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-article-preview',
  templateUrl: './article-preview.component.html',
  styleUrls: ['./article-preview.component.scss']
})
export class ArticlePreviewComponent {
  @Input() imagePath: string = '';
  @Input() title: string = '';
  @Input() description: string = '';
}
