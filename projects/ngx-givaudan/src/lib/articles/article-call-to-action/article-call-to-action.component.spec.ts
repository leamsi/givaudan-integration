import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ArticleCallToActionComponent} from './article-call-to-action.component';

describe('ArticleCallToActionComponent', () => {
  let component: ArticleCallToActionComponent;
  let fixture: ComponentFixture<ArticleCallToActionComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ArticleCallToActionComponent]
    });
    fixture = TestBed.createComponent(ArticleCallToActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
