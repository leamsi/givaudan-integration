import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-article-call-to-action',
  templateUrl: './article-call-to-action.component.html',
  styleUrls: ['./article-call-to-action.component.scss']
})
export class ArticleCallToActionComponent {
  @Input() article!: { title: string; descriptions: string []; buttonLabel: string; };
}
