import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent {
  @Input() width: '102px' | string = '102px';
  @Input() icon: 'search' | '' = '';
  @Output() change: EventEmitter<string> = new EventEmitter<string>();
  input: string = '';

  onChange() {
    this.change.emit(this.input);
  }
}
