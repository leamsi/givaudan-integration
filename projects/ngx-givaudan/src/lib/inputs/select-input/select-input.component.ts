import {AfterViewInit, Component, Input} from '@angular/core';

@Component({
  selector: 'app-select-input',
  templateUrl: './select-input.component.html',
  styleUrls: ['./select-input.component.scss']
})
export class SelectInputComponent implements AfterViewInit {
    @Input() width: '102px' | '170px' | '112px' | string = '102px';
    @Input() values!: SelectInputModel [];

  ngAfterViewInit(): void {
  }
}

export interface SelectInputModel {
  label: string | number;
  value?: string | number;
}
