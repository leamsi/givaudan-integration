import {Meta, StoryObj} from '@storybook/angular';
import {CommentOutlineIconComponent} from 'ngx-givaudan';

const meta: Meta<CommentOutlineIconComponent> = {
  component: CommentOutlineIconComponent,
  args: {
    color: 'black'
  }
}

export default meta;
type Story = StoryObj<CommentOutlineIconComponent>;

export const CommentOutlineIcon: Story = {};
export const CommentOutlineIconCustomColor: Story = {
  args: {
    color: '#549DAA'
  }
};
