import {Meta, StoryObj} from '@storybook/angular';
import {SearchIconComponent} from 'ngx-givaudan';

const meta: Meta<SearchIconComponent> = {
  component: SearchIconComponent
}

export default meta;
type Story = StoryObj<SearchIconComponent>;

export const SearchIcon: Story = {};
export const SearchIconWhite: Story = {
  args: {
    color: 'white'
  }
};
export const SearchIconCustomColor: Story = {
  args: {
    color: 'yellow'
  }
};
