import {Meta, StoryObj} from '@storybook/angular';
import {ChevronDownIconComponent} from 'ngx-givaudan';

const meta: Meta<ChevronDownIconComponent> = {
  component: ChevronDownIconComponent,
  args: {
    color: 'black'
  }
}

export default meta;
type Story = StoryObj<ChevronDownIconComponent>;

export const ChevronDownIcon: Story = {}

export const ChevronDownIconCustomColor: Story = {
  args: {
    color: '#549DAA'
  }
}

export const ChevronDownIconUp: Story = {
  args: {
    direction: 'up'
  }
}

export const ChevronDownIconRight: Story = {
  args: {
    direction: 'right'
  }
}

export const ChevronDownIconLeft: Story = {
  args: {
    direction: 'left'
  }
}
