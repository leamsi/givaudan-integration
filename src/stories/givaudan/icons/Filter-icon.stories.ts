import {Meta, StoryObj} from '@storybook/angular';
import {FilterIconComponent} from 'ngx-givaudan';

const meta: Meta<FilterIconComponent> = {
  component: FilterIconComponent
}

export default meta;
type Story = StoryObj<FilterIconComponent>;

export const FilterIcon: Story = {};
export const FilterIconCustomWidth12Px: Story = {
  args: {
    width: '12px'
  }
};
