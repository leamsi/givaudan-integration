import {Meta, StoryObj} from '@storybook/angular';
import {FavoriteOutlineIconComponent} from 'ngx-givaudan';

const meta: Meta<FavoriteOutlineIconComponent> = {
  component: FavoriteOutlineIconComponent,
  args: {
    color: 'black'
  }
}

export default meta;
type Story = StoryObj<FavoriteOutlineIconComponent>;

export const FavoriteOutlineIcon: Story = {};
export const FavoriteOutlineIconCustomColor: Story = {
  args: {
    color: '#549DAA'
  }
};
