import {Meta, StoryObj} from '@storybook/angular';
import {HelpCircleIconComponent} from 'ngx-givaudan';

const meta: Meta<HelpCircleIconComponent> = {
  component: HelpCircleIconComponent,
  args: {
    color: 'black'
  }
}

export default meta;
type Story = StoryObj<HelpCircleIconComponent>;

export const HelpCircleIcon: Story = {};
export const HelpCircleIconCustomWhite: Story = {
  args: {
    color: 'white'
  }
};
