import {Meta, StoryObj} from '@storybook/angular';
import {ArrowForwardIconComponent} from 'ngx-givaudan';

const meta: Meta<ArrowForwardIconComponent> = {
  component: ArrowForwardIconComponent
}

export default meta;
type Story = StoryObj<ArrowForwardIconComponent>;

export const ArrowForwardIcon: Story = {};
export const ArrowForwardIconCustomColor: Story = {
  args: {
    color: '#549DAA'
  }
};

export const RotateArrowForwardIcon: Story = {
  args: {
    rotate: true,
    color: '#549DAA'
  }
};
