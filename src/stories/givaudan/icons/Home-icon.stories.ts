import {Meta, StoryObj} from '@storybook/angular';
import {HomeIconComponent} from 'ngx-givaudan';

const meta: Meta<HomeIconComponent> = {
  component: HomeIconComponent,
  args: {
    color: '#549DAA'
  }
}

export default meta;
type Story = StoryObj<HomeIconComponent>;

export const HomeIcon: Story = {};
export const HomeIconWhite: Story = {
  args: {
    color: 'white'
  }
};

