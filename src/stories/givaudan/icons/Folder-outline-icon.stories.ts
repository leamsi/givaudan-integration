import {Meta, StoryObj} from '@storybook/angular';
import {FolderOutlineIconComponent} from 'ngx-givaudan';

const meta: Meta<FolderOutlineIconComponent> = {
  component: FolderOutlineIconComponent,
  args: {
    color: 'black'
  }
}

export default meta;
type Story = StoryObj<FolderOutlineIconComponent>;

export const FolderOutlineIcon: Story = {};

export const FolderOutlineIconWhiteColor: Story = {
  args: {
    color: 'white'
  }
};

export const FolderOutlineIconCustomColor: Story = {
  args: {
    color: '#de8787'
  }
};
