import {Meta, StoryObj} from '@storybook/angular';
import {ButtonComponent} from 'ngx-givaudan';

const meta: Meta<ButtonComponent> = {
  component: ButtonComponent,
  args: { label: 'Button'},
  argTypes: {clickEvent: {action: 'clicked'}}
}

export default meta;
type Story = StoryObj<ButtonComponent>;

export const DefaultButton: Story = {
  args: {},
};

export const ButtonCustomColor: Story = {
  args: {
    color: 'yellow',
    background: 'purple'
  },
};

export const ButtonDisabled: Story = {
  args: {
    disabled: true
  },
};

export const ButtonCustomWidth: Story = {
  args: {
    width: '200px'
  },
};
