import {Meta, StoryObj} from '@storybook/angular';
import {ChevronDownIconComponent} from 'ngx-givaudan';

const meta: Meta<ChevronDownIconComponent> = {
  component: ChevronDownIconComponent
}

export default meta;
type Story = StoryObj<ChevronDownIconComponent>;

export const DefaultChevronDownIcon: Story = {}
