import {Meta, moduleMetadata, StoryObj} from '@storybook/angular';
import {InputComponent, NgxGivaudanModule} from 'ngx-givaudan';

const meta: Meta<InputComponent> = {
  component: InputComponent,
  argTypes: {change: {action: 'input value'}},
  decorators: [
    moduleMetadata({
      imports: [NgxGivaudanModule],
    })],
}

export default meta;
type Story = StoryObj<InputComponent>;

export const DefaultInput: Story = {}
export const DefaultInputWithIcon: Story = {
  args: {
    icon: 'search'
  }
}
export const DefaultInputWithCustomWith: Story = {
  args: {
    width: '500px',
    icon: 'search'
  }
}
