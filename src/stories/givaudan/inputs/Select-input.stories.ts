import {Meta, StoryObj} from '@storybook/angular';
import {SelectInputComponent} from 'ngx-givaudan';

const meta: Meta<SelectInputComponent> = {
  component: SelectInputComponent,
}

export default meta;
type Story = StoryObj<SelectInputComponent>;

export const SelectInput: Story = {
  args: {
    values: [{label: 'Alpha'}, {label: 2023}]
  }
}
