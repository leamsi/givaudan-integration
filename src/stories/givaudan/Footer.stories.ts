import {Meta, StoryObj} from '@storybook/angular';
import {FooterComponent} from 'ngx-givaudan';

const meta: Meta<FooterComponent> = {
  component: FooterComponent,
  args: {
    supportLogo: '/assets/images/support-footer-logo.png'
  }
}

export default meta;
type Story = StoryObj<FooterComponent>;

export const Footer: Story = {}
