import {Meta, moduleMetadata, StoryObj} from '@storybook/angular';
import {ArticleCallToActionComponent, NgxGivaudanModule} from 'ngx-givaudan';

const meta: Meta<ArticleCallToActionComponent> = {
  component: ArticleCallToActionComponent,
  args: {
    article: {title: 'Lorem ipsum', descriptions:   [
        'ANunc volutpat nec velit a gravida. Suspendisse imperdiet nisl ac vestibulum elementum.',
        'Nam vehicula eros ut placerat mollis.',
        'Morbi sed risus vel nibh auctor imperdiet et ac leo.'
      ], buttonLabel: 'Go to Lorem ipsum'}
  },
  decorators: [
    moduleMetadata({
      imports: [NgxGivaudanModule],
    })],
}

export default meta;
type Story = StoryObj<ArticleCallToActionComponent>;

export const ArticleToAction: Story = {}
