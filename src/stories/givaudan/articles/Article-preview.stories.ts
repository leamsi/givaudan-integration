import {Meta, StoryObj} from '@storybook/angular';
import {ArticlePreviewComponent} from 'ngx-givaudan';

const meta: Meta<ArticlePreviewComponent> = {
  component: ArticlePreviewComponent,
  args: {
    imagePath: 'https://media.istockphoto.com/id/473082752/fr/photo/des-fleurs-fra%C3%AEches-dans-un-cornet-de-glace-nature-morte.jpg?s=2048x2048&w=is&k=20&c=F8CIxuvy4k9o0hpwLs5qnMCHaiGJJ_R5iygZ8IvN8i4=',
    title: 'Lorem ipsum dolor sit amet',
    description: "<p>ANunc volutpat nec velit a gravida. Suspendisse imperdiet nisl ac vestibulum elementum." +
      " Nam vehicula eros ut placerat mollis. Morbi sed risus vel nibh auctor imperdiet et ac leo. Nullam posuere " +
      "sodales turpis, sed ultricies risus hendrerit ut. Mauris finibus lectus ut turpis dictum, eu convallis lorem laoreet.</p>"
  }
}

export default meta;
type Story = StoryObj<ArticlePreviewComponent>;

export const ArticlePreview: Story = {}
export const ArticleSecondPreview: Story = {
  args:     {
    imagePath: 'https://media.istockphoto.com/id/473082752/fr/photo/des-fleurs-fra%C3%AEches-dans-un-cornet-de-glace-nature-morte.jpg?s=2048x2048&w=is&k=20&c=F8CIxuvy4k9o0hpwLs5qnMCHaiGJJ_R5iygZ8IvN8i4=',
    title: 'Lorem ipsum dolor sit amet',
    description: "<ul><li>ANunc volutpat nec velit a gravida. Suspendisse imperdiet nisl ac vestibulum elementum.</li>" +
      "<li>Nam vehicula eros ut placerat mollis.</li>" +
      "<li>Morbi sed risus vel nibh auctor imperdiet et ac leo. Nullam posuere sodales turpis, sed ultricies risus hendrerit ut.</li>" +
      "<li>Mauris finibus lectus ut turpis dictum, eu convallis lorem laoreet.</li></ul>"
  }
}
