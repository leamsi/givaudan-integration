import {Meta, moduleMetadata, StoryObj} from '@storybook/angular';
import {LineChartComponent} from 'ngx-givaudan';
import {NgChartsModule} from 'ng2-charts';

const meta: Meta<LineChartComponent> = {
  component: LineChartComponent,
  decorators: [
    moduleMetadata({
      imports: [NgChartsModule],
    })],
  argTypes: {selectedEvent: {action: 'selected value'}}
}

export default meta;
type Story = StoryObj<LineChartComponent>;

export const LineChartWithOutData: Story = {}
export const LineChartWithData: Story = {
  args: {
    chart: {
      labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
      series: [
        { label: 'Series A', data: [10, 6, 1, 28, 9, 13, 4] },
        { label: 'Series B', data: [3, 6, 4, 19, 25, 8, 17] },
        { label: 'Series C', data: [3, 12, 6, 1, 15, 17, 9] }
      ]
    }
  }
}
