import {Meta, moduleMetadata, StoryObj} from '@storybook/angular';
import {BubbleChartComponent} from 'ngx-givaudan';
import {NgChartsModule} from 'ng2-charts';

const meta: Meta<BubbleChartComponent> = {
  component: BubbleChartComponent,
  decorators: [
    moduleMetadata({
      imports: [NgChartsModule],
    })]
}

export default meta;
type Story = StoryObj<BubbleChartComponent>;

export const BubbleChartWithOutData: Story = {}
