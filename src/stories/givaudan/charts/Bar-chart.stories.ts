import {Meta, moduleMetadata, StoryObj} from '@storybook/angular';
import {BarChartComponent} from 'ngx-givaudan';
import {NgChartsModule} from 'ng2-charts';

const meta: Meta<BarChartComponent> = {
  component: BarChartComponent,
  decorators: [
    moduleMetadata({
      imports: [NgChartsModule],
    })]
}

export default meta;
type Story = StoryObj<BarChartComponent>;

export const BarChartWithOutData: Story = {}
