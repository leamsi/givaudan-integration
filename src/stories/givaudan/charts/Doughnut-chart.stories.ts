import {Meta, moduleMetadata, StoryObj} from '@storybook/angular';
import {DoughnutChartComponent} from 'ngx-givaudan';
import {NgChartsModule} from 'ng2-charts';

const meta: Meta<DoughnutChartComponent> = {
  component: DoughnutChartComponent,
  decorators: [
    moduleMetadata({
      imports: [NgChartsModule],
    })]
}

export default meta;
type Story = StoryObj<DoughnutChartComponent>;

export const DoughnutChartWithOutData: Story = {}
