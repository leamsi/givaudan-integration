import {Meta, StoryObj} from '@storybook/angular';
import {HeaderComponent} from 'ngx-givaudan';

const meta: Meta<HeaderComponent> = {
  component: HeaderComponent,
  args: {
    profilePicture: '/assets/images/profil-picture.png'
  }
}

export default meta;
type Story = StoryObj<HeaderComponent>;

export const Header: Story = {}
