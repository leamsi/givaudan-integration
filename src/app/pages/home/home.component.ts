import {AfterViewInit, Component, ElementRef, Inject, NgZone, ViewChild} from '@angular/core';
import Swiper from 'swiper';
import * as _ from 'lodash';
import {DOCUMENT} from '@angular/common';
import {BarChartComponent, BubbleChartComponent, DoughnutChartComponent, LineChartComponent} from 'ngx-givaudan';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements AfterViewInit {
  @ViewChild(LineChartComponent) lineChartComponent!: LineChartComponent;
  @ViewChild(BarChartComponent) barChartComponent!: BarChartComponent;
  @ViewChild(DoughnutChartComponent) doughnutChartComponent!: DoughnutChartComponent;
  @ViewChild(BubbleChartComponent) bubbleChartComponent!: BubbleChartComponent;
  @ViewChild('firstChartSwiper', { static: false }) swiperRef!: ElementRef<{ swiper: Swiper }>
  charts: {label: string; types: ('line' | 'bar' | 'doughnut' | 'bubble') []} [] = [
    {label: 'Charts 1', types: ['line','bar','doughnut']},
    {label: 'Charts 2', types: ['bubble'] }
  ];
  currentChart: number = 0;
  articleResume: boolean = true;

  lineChart: {labels: string []; series: {label: string; data: number []} []} = {
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    series: [
      { label: 'Series A', data: [10, 6, 1, 28, 9, 13, 4] },
      { label: 'Series B', data: [3, 6, 4, 19, 25, 8, 17] },
      { label: 'Series C', data: [3, 12, 6, 1, 15, 17, 9] }
    ]
  }
  private currentSwiperIndex = 0;
  articles: {imagePath: string; title: string; description: string}[] = [
    {
      imagePath: 'assets/images/article-img-01.png',
      title: 'Lorem ipsum dolor sit amet',
      description: "<p>ANunc volutpat nec velit a gravida. Suspendisse imperdiet nisl ac vestibulum elementum." +
        " Nam vehicula eros ut placerat mollis. Morbi sed risus vel nibh auctor imperdiet et ac leo. Nullam posuere " +
        "sodales turpis, sed ultricies risus hendrerit ut. Mauris finibus lectus ut turpis dictum, eu convallis lorem laoreet.</p>"
    },
    {
      imagePath: 'assets/images/article-img-02.png',
      title: 'Lorem ipsum dolor sit amet',
      description: "<ul><li>ANunc volutpat nec velit a gravida. Suspendisse imperdiet nisl ac vestibulum elementum.</li>" +
        "<li>Nam vehicula eros ut placerat mollis.</li>" +
        "<li>Morbi sed risus vel nibh auctor imperdiet et ac leo. Nullam posuere sodales turpis, sed ultricies risus hendrerit ut.</li>" +
        "<li>Mauris finibus lectus ut turpis dictum, eu convallis lorem laoreet.</li></ul>"
    }
  ];

  constructor(private ngZone: NgZone, @Inject(DOCUMENT) private document: Document) {}

  exportChart($event: boolean) {
    if ($event) {
      switch (this.currentChart) {
        case 0:
          switch (this.getSelectedChartTypes()[this.currentSwiperIndex]) {
            case 'line':
              this.lineChartComponent.downloadChart();
              break;
            case 'bar':
              this.barChartComponent.downloadChart();
              break;
            case 'doughnut':
              this.doughnutChartComponent.downloadChart();
              break;
          }
          break;
        case 1:
          this.bubbleChartComponent.downloadChart();
          break;
      }
    }
  }

  goToChartType(chartType: string) {
    const chartTypeIndex = this.getSelectedChartTypes().findIndex(wantedChartType => wantedChartType === chartType);
    this.slideTo(chartTypeIndex);
  }

  ngAfterViewInit(): void {
    this.listenSlideChangeTransitionEnd();
  }

  isSelectedChartType(chartType: string): boolean {
    return this.getSelectedChartTypes()[this.currentSwiperIndex] === chartType;
  }

  resumeArticleToggle() {
    this.articleResume = !this.articleResume;
  }

  isSelectedChart(chart: { label: string; types: string[] }) {
    return _.isEqual(this.charts[this.currentChart], chart);
  }

  selectChart(chart: { label: string; types: string[] }) {
    this.currentChart = this.charts.findIndex(wantedChart => wantedChart.label === chart.label);
    this.currentSwiperIndex = 0;
    if (this.currentChart === 0 && this.swiperRef?.nativeElement.swiper.activeIndex !== 0) {
      this.slideTo(0)
    }
  }

  getSelectedChartTypes(): string [] {
    return this.charts[this.currentChart].types;
  }


  private slideTo(chartTypeIndex: number) {
    this.swiperRef?.nativeElement.swiper.slideTo(chartTypeIndex);
  }

  private listenSlideChangeTransitionEnd() {
    this.swiperRef?.nativeElement.swiper.on('slideChangeTransitionEnd', () => {
      this.ngZone.run(() => {
        this.currentSwiperIndex = this.swiperRef?.nativeElement.swiper.activeIndex ?? 0;
      });
    });
  }
}
